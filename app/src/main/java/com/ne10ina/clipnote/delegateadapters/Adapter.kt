package com.ne10ina.clipnote.delegateadapters

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.DiffUtil
import com.ne10ina.clipnote.MainActivity
import com.ne10ina.clipnote.State
import kotlinx.android.synthetic.main.activity_main.*
import ru.touchin.adapters.DelegationListAdapter

class Adapter(val context: Context) : DelegationListAdapter<RecyclerItems>(CALLBACK) {

    lateinit var data: MutableList<RecyclerItems>
    var isSwiped = false

    companion object {
        private val CALLBACK = object : DiffUtil.ItemCallback<RecyclerItems>() {
            override fun areItemsTheSame(oldItem: RecyclerItems, newItem: RecyclerItems) = oldItem == newItem
            override fun areContentsTheSame(oldItem: RecyclerItems, newItem: RecyclerItems) = false
        }
    }

    fun refresh(list: Map<State, MutableList<RecyclerItems>>) {
        val temp: MutableList<RecyclerItems> = mutableListOf()

        if (list[State.PINED]?.size != 1)
            temp.addAll(list[State.PINED]!!.distinct())
        if (list[State.ACTIVE]?.size != 1)
            temp.addAll(list[State.ACTIVE]!!.distinct())
        if (list[State.ARCHIVED]?.size != 1)
            temp.addAll(list[State.ARCHIVED]!!.distinct())
        if (temp.size == 0) {
            (context as MainActivity).noNoteView.visibility = View.VISIBLE
        } else {
            (context as MainActivity).noNoteView.visibility = View.GONE
        }


        data = temp
        submitList(temp)
        isSwiped = false
    }

    init {
        addDelegate(NoteDelegate(context))
        addDelegate(DivisorDelegate())
    }
}