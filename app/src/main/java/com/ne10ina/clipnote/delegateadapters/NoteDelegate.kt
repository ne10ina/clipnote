package com.ne10ina.clipnote.delegateadapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.ConfigurationCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.ne10ina.clipnote.*
import com.ne10ina.clipnote.delegateadapters.adapters.ItemAdapterDelegate
import com.ne10ina.clipnote.utils.Utils
import kotlinx.android.synthetic.main.item_card.view.*
import java.text.SimpleDateFormat
import java.util.*

class NoteDelegate(var context: Context) : ItemAdapterDelegate<NoteDelegate.NoteHolder, RecyclerItems.NoteItem>() {

    override fun onCreateViewHolder(parent: ViewGroup): NoteHolder {
        return NoteHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_card,
                parent,
                false
            )
        )
    }

    private fun startActivity(adapterPosition: Int) {
        val activity: MainActivity = context as MainActivity
        activity.startAddNoteActivity(adapterPosition)
    }

    override fun onBindViewHolder(
        holder: NoteHolder,
        item: RecyclerItems.NoteItem,
        adapterPosition: Int,
        collectionPosition: Int,
        payloads: MutableList<Any>
    ) {
        holder.cardText.text = item.text
        holder.state = item.state
        holder.context = context
        holder.type = item.type
        holder.colorLayout.setBackgroundColor(item.color)
        holder.cardLayout.setOnClickListener { startActivity(adapterPosition) }
        holder.typeImage.setOnClickListener { startActivity(adapterPosition) }
        holder.cardText.setOnClickListener { startActivity(adapterPosition) }
        holder.textTime.setOnClickListener { startActivity(adapterPosition) }
        holder.typeImage.background = Utils.getIcon(item.type, context)

        when {
            item.type == Type.ONE -> {
                val dateFormat = SimpleDateFormat(
                    "dd.MM.yy в HH:mm",
                    ConfigurationCompat.getLocales(context.resources.configuration)[0]
                )
                holder.textTime.text = dateFormat.format(Date(item.timeToDo))
            }
            else -> {
                val df = SimpleDateFormat(
                    "HH:mm",
                    ConfigurationCompat.getLocales(context.resources.configuration)[0]
                )
                holder.textTime.text = df.format(Date(item.time))
            }
        }
    }

    override fun isForViewType(item: Any, adapterPosition: Int, collectionPosition: Int): Boolean {
        return item is RecyclerItems.NoteItem
    }

    class NoteHolder(view: View) : RecyclerView.ViewHolder(view) {
        lateinit var context: Context
        val cardText = itemView.cardtext
        val textTime = itemView.textTime
        val cardLayout = itemView.cardLayout
        val typeImage = itemView.typeImage
        var state = State.ACTIVE
        var type = Type.DEFAULT
        var colorLayout = itemView.colorLayout

        private fun doWork(activity: MainActivity, oldState: State, newState: State, newType: Type) {
            val note = activity.adapter.data[adapterPosition] as RecyclerItems.NoteItem
            activity.list[oldState]!!.remove(note)

            note.type = newType
            note.state = newState
            activity.list[newState]!!.add(1, note)

            Utils.updateItem(note, activity.database)
            Utils.smoothScroll(context, Utils.getPositionToScroll(newState, activity), activity.recyclerView)
        }

        private fun archive(activity: MainActivity, state: State) {
            val note = activity.adapter.data[adapterPosition] as RecyclerItems.NoteItem
            Utils.deleteNotification(note.id, context)
            Utils.deleteTask(context, note.text, note.tasksId, note.color)

            activity.list[state]!!.remove(note)

            val df = SimpleDateFormat(
                "HH:mm",
                ConfigurationCompat.getLocales(context.resources.configuration)[0]
            )
            textTime.text = df.format(Date(note.time))

            note.state = State.ARCHIVED
            note.type = Type.DEFAULT
            note.timeToDo = -1
            activity.list[State.ARCHIVED]!!.add(1, note)

            Utils.updateItem(note, activity.database)
            Utils.smoothScroll(context, Utils.getPositionToScroll(State.ARCHIVED, activity), activity.recyclerView)
        }

        fun onSwiped(direction: Int) {
            val activity = context as MainActivity
            if(activity.adapter.data[adapterPosition] is RecyclerItems.DivisorItem)
                return
            val item = (activity.adapter.data[adapterPosition] as RecyclerItems.NoteItem)

            if (direction == ItemTouchHelper.LEFT) {
                when (state) {
                    State.PINED -> {
                        //Из пина в архив
                        archive(activity, State.PINED)
                    }
                    State.ACTIVE -> {
                        //Из активных в архив
                        archive(activity, State.ACTIVE)
                    }
                    State.ARCHIVED -> {
                        //Полное удаление
                        val note = activity.adapter.data[adapterPosition] as RecyclerItems.NoteItem
                        Utils.deleteItem(note, activity.database)
                        activity.list[State.ARCHIVED]!!.remove(note)
                    }
                }
                activity.adapter.refresh(activity.list)
                return
            }

            when (state) {
                State.ARCHIVED -> {
                    //Восстановление
                    doWork(activity, State.ARCHIVED, State.ACTIVE, Type.DEFAULT)
                }
                State.ACTIVE -> {
                    //Пин
                    doWork(activity, State.ACTIVE, State.PINED, type)
                    if ((activity.adapter.data[adapterPosition] as RecyclerItems.NoteItem).type != Type.PINED)
                        Utils.addStableNotification(item.text, item.id, item.color, context)
                }
                State.PINED -> {
                    //Анпин
                    doWork(activity, State.PINED, State.ACTIVE, type)
                    if ((activity.adapter.data[adapterPosition] as RecyclerItems.NoteItem).type != Type.PINED)
                        Utils.deleteNotification(
                            (activity.adapter.data[adapterPosition] as RecyclerItems.NoteItem).id,
                            context
                        )
                }
            }
            activity.adapter.refresh(activity.list)
        }
    }
}