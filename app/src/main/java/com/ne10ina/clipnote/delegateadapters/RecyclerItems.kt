package com.ne10ina.clipnote.delegateadapters

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ne10ina.clipnote.State
import com.ne10ina.clipnote.Type
import java.io.Serializable

abstract class RecyclerItems {
    class DivisorItem(var text: String) : RecyclerItems()

    @Entity(tableName = "notes")
    class NoteItem(
        @ColumnInfo(name = "id")
        @PrimaryKey(autoGenerate = true)
        var id: Int,
        @ColumnInfo(name = "tasksId")
        var tasksId: Int,
        @ColumnInfo(name = "state")
        var state: State,
        @ColumnInfo(name = "text")
        var text: String,
        @ColumnInfo(name = "time")
        var time: Long,
        @ColumnInfo(name = "type")
        var type: Type,
        @ColumnInfo(name = "timeToDo")
        var timeToDo: Long,
        @ColumnInfo(name = "color")
        var color: Int
    ) :
        RecyclerItems(), Serializable
}