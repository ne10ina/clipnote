package com.ne10ina.clipnote.delegateadapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ne10ina.clipnote.R
import com.ne10ina.clipnote.delegateadapters.adapters.ItemAdapterDelegate
import kotlinx.android.synthetic.main.item_dividor.view.*

class DivisorDelegate : ItemAdapterDelegate<DivisorDelegate.DivisorHolder, RecyclerItems.DivisorItem>() {
    override fun onCreateViewHolder(parent: ViewGroup): DivisorHolder {
        return DivisorHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_dividor,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(
        holder: DivisorHolder,
        item: RecyclerItems.DivisorItem,
        adapterPosition: Int,
        collectionPosition: Int,
        payloads: MutableList<Any>
    ) {
        holder.divisor.text = item.text

    }

    override fun isForViewType(item: Any, adapterPosition: Int, collectionPosition: Int): Boolean {
        return item is RecyclerItems.DivisorItem
    }

    class DivisorHolder(view: View) : RecyclerView.ViewHolder(view) {
        var divisor = itemView.dividerTextView
    }
}