package com.ne10ina.clipnote

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import com.ne10ina.clipnote.delegateadapters.RecyclerItems
import com.ne10ina.clipnote.utils.Utils
import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import android.os.Build
import android.widget.Toast


class BroadcastReceiver : BroadcastReceiver() {
    lateinit var context1: Context

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.getIntExtra("code", -1) == 1) {
            Utils.addNotification(
                intent.getStringExtra("text"),
                intent.getIntExtra("id", 0),
                intent.getIntExtra("color", Color.parseColor("#ffffff")),
                context
            )
        } else if (intent.getIntExtra("code", -1) == 2) {
            val activity = context1 as MainActivity
            val result: MutableMap<State, MutableList<RecyclerItems>> =
                intent.getSerializableExtra("items") as MutableMap<State, MutableList<RecyclerItems>>

            activity.list[State.PINED]?.addAll(result[State.PINED]!!)
            activity.list[State.ACTIVE]?.addAll(result[State.ACTIVE]!!)
            activity.list[State.ARCHIVED]?.addAll(result[State.ARCHIVED]!!)
            activity.adapter.refresh(activity.list)
        } else {
            val intent = Intent(context, RunAfterBootService::class.java)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent)
            } else {
                context.startService(intent)
            }
        }
    }
}

class MyResultReceiver(handler: Handler) : ResultReceiver(handler) {

    var resultValue: GetResultInterface? = null

    override fun onReceiveResult(resultCode: Int, resultData: Bundle) {
        resultValue?.getResult(resultCode, resultData)
    }

    fun setReceiver(result: GetResultInterface) {
        resultValue = result
    }

    interface GetResultInterface {
        fun getResult(resultCode: Int, resultData: Bundle)
    }


}