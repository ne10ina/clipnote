package com.ne10ina.clipnote

import android.app.Service
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.room.Room
import com.ne10ina.clipnote.db.AppDatabase
import com.ne10ina.clipnote.delegateadapters.RecyclerItems
import com.ne10ina.clipnote.utils.Utils
import java.lang.Exception


class RunAfterBootService : Service() {

    lateinit var database: AppDatabase

    override fun onBind(intent: Intent): IBinder {
        throw UnsupportedOperationException("Not yet implemented")
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        var items = mutableListOf<RecyclerItems.NoteItem>()
        try {
            database = Room.databaseBuilder(
                applicationContext,
                AppDatabase::class.java, "note-database"
            ).allowMainThreadQueries().build()

            items = database.noteDao().getAll()
        }
        catch (e: Exception)
        {
            Log.e("Error", e.message)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            Utils.createNotificationChannel(this)

        for(i in 0 until items.size)
        {
            if(items[i].type == Type.PINED || items[i].state == State.PINED)
            {
                Utils.addStableNotification(items[i].text, items[i].id, items[i].color, this)
            }
            if(items[i].type == Type.ONE)
            {
                Utils.addTask(this, items[i].id, items[i].text, items[i].color, items[i].timeToDo)
            }
        }

        return super.onStartCommand(intent, flags, startId)
    }
}