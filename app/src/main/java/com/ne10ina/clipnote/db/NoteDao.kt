package com.ne10ina.clipnote.db

import androidx.room.*
import com.ne10ina.clipnote.State
import com.ne10ina.clipnote.Type
import com.ne10ina.clipnote.delegateadapters.RecyclerItems

@Dao
interface NoteDao {
    @Query("SELECT * FROM notes")
    fun getAll(): MutableList<RecyclerItems.NoteItem>

    @Query("SELECT * FROM notes WHERE state = (:state)")
    fun getAllByState(state: State): MutableList<RecyclerItems.NoteItem>

    @Query("SELECT * FROM notes WHERE id = (:id) LIMIT 1")
    fun getNoteById(id: Int): RecyclerItems.NoteItem

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateNote(note: RecyclerItems.NoteItem)

    @Insert
    fun addNote(note: RecyclerItems.NoteItem): Long

    @Delete
    fun deleteNote(note: RecyclerItems.NoteItem)
}

@Database(entities = [RecyclerItems.NoteItem::class], version = 1)
@TypeConverters(StateConverter::class, TypesConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun noteDao(): NoteDao
}

class TypesConverter {

    @TypeConverter
    fun toType(type: String): Type =
        when (type) {
            Type.PINED.name -> Type.PINED
            Type.ONE.name -> Type.ONE
            else -> Type.DEFAULT
        }

    @TypeConverter
    fun fromType(type: Type): String = type.name
}

class StateConverter {

    @TypeConverter
    fun toState(state: String): State =
        when (state) {
            State.PINED.name -> State.PINED
            State.ARCHIVED.name -> State.ARCHIVED
            else -> State.ACTIVE
        }

    @TypeConverter
    fun fromState(state: State): String = state.name
}