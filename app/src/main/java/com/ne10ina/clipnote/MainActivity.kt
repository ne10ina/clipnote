package com.ne10ina.clipnote

import android.app.*
import android.content.*
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ne10ina.clipnote.delegateadapters.Adapter
import com.ne10ina.clipnote.delegateadapters.RecyclerItems
import com.ne10ina.clipnote.utils.Utils
import java.io.Serializable
import kotlin.random.Random.Default.nextInt
import android.graphics.Color
import android.os.Build
import android.os.Handler
import androidx.room.Room
import com.ne10ina.clipnote.db.AppDatabase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*


const val REQUSETCODE = 228

class MainActivity : AppCompatActivity(), MyResultReceiver.GetResultInterface {
    lateinit var adapter: Adapter
    lateinit var recyclerView: RecyclerView
    lateinit var database: AppDatabase
    var myResultReceiver: MyResultReceiver? = null
    private lateinit var broadcastReceiver: BroadcastReceiver
    val list: MutableMap<State, MutableList<RecyclerItems>> = mutableMapOf(
        State.PINED to mutableListOf(),
        State.ACTIVE to mutableListOf(),
        State.ARCHIVED to mutableListOf()
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            Utils.createNotificationChannel(this)
        addNoteTextView.setOnClickListener { startAddNoteActivity(-1) }
        addNoteButton.setOnClickListener { startAddNoteActivity(-1) }
        aboutImage.setOnClickListener { showAboutDialog() }
        database = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "note-database"
        ).build()
        broadcastReceiver = BroadcastReceiver()
        broadcastReceiver.context1 = this
        myResultReceiver = MyResultReceiver(Handler())
        myResultReceiver!!.setReceiver(this)
        val layoutManager = LinearLayoutManager(this)
        recyclerView = RLayout
        recyclerView.layoutManager = layoutManager
        adapter = Adapter(this)
        recyclerView.adapter = adapter
        loadNotes()

        val dividerItemDecoration = VerticalSpaceItemDecoration(Utils.dpToPx(10))
        recyclerView.addItemDecoration(dividerItemDecoration)
        val itemTouchHelper = ItemTouchHelper(SwipeToDeleteCallback(this))
        itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    private fun showAboutDialog() {
        val alertDialog = AlertDialog.Builder(this@MainActivity).create()
        alertDialog.setTitle("Информация")
        alertDialog.setMessage("Разработчк: Игорь Лизунов \n\nСвязь Telegram: @ne10ina \n\nLKL/2019")
        alertDialog.setButton(
            AlertDialog.BUTTON_NEUTRAL, "Закрыть"
        ) { dialog, _ -> dialog.dismiss() }
        alertDialog.show()
    }

    private fun loadNotes() {
        list[State.PINED]?.add(RecyclerItems.DivisorItem(getString(R.string.pin)))
        list[State.ACTIVE]?.add(RecyclerItems.DivisorItem(getString(R.string.active)))
        list[State.ARCHIVED]?.add(RecyclerItems.DivisorItem(getString(R.string.archived)))

        GlobalScope.launch(Dispatchers.IO) {
            val result: MutableMap<State, MutableList<RecyclerItems>> = mutableMapOf(
                State.PINED to mutableListOf(),
                State.ACTIVE to mutableListOf(),
                State.ARCHIVED to mutableListOf()
            )
            runBlocking(Dispatchers.IO) {
                result[State.PINED]!!.addAll(database.noteDao().getAllByState(State.PINED))
                result[State.ACTIVE]!!.addAll(database.noteDao().getAllByState(State.ACTIVE))
                result[State.ARCHIVED]!!.addAll(database.noteDao().getAllByState(State.ARCHIVED))
            }

            val bundle = Bundle()
            bundle.putSerializable("result", result as Serializable)
            myResultReceiver!!.send(100, bundle)
        }
    }

    fun startAddNoteActivity(position: Int) {
        if (adapter.isSwiped)
            return
        adapter.isSwiped = true
        val intent = Intent(this, AddNote::class.java)
        if (position != -1) {
            val item = adapter.data[position] as RecyclerItems.NoteItem
            intent.putExtra("pos", position)
            intent.putExtra("oldC", item.color)
            intent.putExtra("oldT", item.text)
            intent.putExtra("oldS", item.state as Serializable)
            intent.putExtra("item", item as Serializable)
        } else {
            intent.putExtra(
                "item",
                RecyclerItems.NoteItem(
                    nextInt(),
                    1000,
                    State.ACTIVE,
                    "null",
                    0,
                    Type.DEFAULT,
                    -1,
                    Color.rgb(nextInt(256), nextInt(256), nextInt(256))
                ) as Serializable
            )
        }
        startActivityForResult(intent, REQUSETCODE)
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUSETCODE && resultCode == Activity.RESULT_OK) {
            val item = data!!.getSerializableExtra("item") as RecyclerItems.NoteItem

            if (data.getIntExtra("pos", -1) != -1) {
                Utils.deleteNotification(item.id, this)
                Utils.deleteTask(
                    this,
                    data.getStringExtra("oldT"),
                    item.tasksId,
                    data.getIntExtra("oldC", 0)
                )

                val oldState: State = (data.getSerializableExtra("oldS") as State)
                list[oldState]!!.remove(adapter.data[data.getIntExtra("pos", 1)])

                when {
                    item.state == State.PINED ->
                        Utils.addStableNotification(item.text, item.id, item.color, this)
                    item.type == Type.ONE ->
                        Utils.addTask(this, item.tasksId, item.text, item.color, item.timeToDo)
                }

                list[item.state]!!.add(1, item)
                adapter.refresh(list)
                Utils.updateItem(item, database)
                adapter.isSwiped = false
                return
            }

            Handler().postDelayed({
                when {
                    item.state == State.PINED ->
                        Utils.addStableNotification(item.text, item.id, item.color, this)
                    item.type == Type.ONE ->
                        Utils.addTask(this, item.tasksId, item.text, item.color, item.timeToDo)
                }
            }, 500)

            Utils.addItem(item, database, this)
            list[item.state]!!.add(1, item)
            adapter.refresh(list)
        }
        adapter.isSwiped = false
    }

    override fun getResult(resultCode: Int, resultData: Bundle) {
        if (resultData != null) {
            when (resultCode) {
                100 -> {
                    var items = resultData.getSerializable("result") as MutableMap<State, MutableList<RecyclerItems>>
                    list[State.PINED]!!.addAll(items[State.PINED]!!)
                    list[State.ACTIVE]!!.addAll(items[State.ACTIVE]!!)
                    list[State.ARCHIVED]!!.addAll(items[State.ARCHIVED]!!)
                    adapter.refresh(list)
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        val intentFilter = IntentFilter("com.myapplication2.android.action.broadcast")
        registerReceiver(broadcastReceiver, intentFilter)
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }
}
