package com.ne10ina.clipnote

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.ne10ina.clipnote.delegateadapters.DivisorDelegate
import com.ne10ina.clipnote.delegateadapters.NoteDelegate
import com.ne10ina.clipnote.utils.Utils

class SwipeToDeleteCallback(var context: Context) :
    ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
    private val background: ColorDrawable = ColorDrawable(Color.RED)
    private val iconDelete: Drawable?
    private val iconPin: Drawable?
    private val iconDeleteForever: Drawable?
    private val iconUnpin: Drawable?
    private val iconRenew: Drawable?

    init {
        iconUnpin = ContextCompat.getDrawable(context, R.drawable.ic_undo_black_24dp)
        iconDelete = ContextCompat.getDrawable(context, R.drawable.ic_delete_black_24dp)
        iconPin = ContextCompat.getDrawable(context, R.drawable.pin)
        iconRenew = ContextCompat.getDrawable(context, R.drawable.ic_autorenew_black_24dp)
        iconDeleteForever = ContextCompat.getDrawable(context, R.drawable.ic_delete_forever_black_24dp)

    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder1: RecyclerView.ViewHolder,
        viewHolder: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    private fun getIconState(viewHolder: RecyclerView.ViewHolder, x: Float): Drawable? {
        val holder = viewHolder as NoteDelegate.NoteHolder
        val icon: Drawable?

        icon = if (x > 0) {
            when {
                holder.state == State.ARCHIVED -> iconRenew
                holder.state == State.PINED -> iconUnpin
                else -> iconPin
            }
        } else {
            when {
                holder.state == State.ARCHIVED -> iconDeleteForever
                else -> iconDelete
            }
        }

        return icon
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        if (viewHolder is DivisorDelegate.DivisorHolder || (context as MainActivity).adapter.isSwiped)
            return

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)

        val itemView = viewHolder.itemView
        val icon = getIconState(viewHolder, dX)
        val backgroundCornerOffset = 100
        var iconMargin = (itemView.height - icon!!.intrinsicHeight) / 2
        iconMargin = Utils.dpToPx(20)
        val iconTop = itemView.top + (itemView.height - icon.intrinsicHeight) / 2
        val iconBottom = iconTop + icon.intrinsicHeight

        if (dX > 0) {
            val iconLeft = itemView.left + iconMargin
            val iconRight = iconLeft + icon.intrinsicWidth
            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom)
            background.color = Color.parseColor("#b55909")
            background.setBounds(
                itemView.left, itemView.top,
                itemView.left + dX.toInt() + backgroundCornerOffset, itemView.bottom
            )
        } else if (dX < 0) {
            val iconLeft = itemView.right - iconMargin - icon.intrinsicWidth
            val iconRight = itemView.right - iconMargin
            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom)
            background.color = Color.parseColor("#b01d0c")
            background.setBounds(
                itemView.right + dX.toInt() - backgroundCornerOffset,
                itemView.top, itemView.right, itemView.bottom
            )
        } else {
            background.setBounds(0, 0, 0, 0)
            icon.setBounds(0, 0, 0, 0)
        }

        background.draw(c)
        icon.draw(c)

    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        if (viewHolder is DivisorDelegate.DivisorHolder || (context as MainActivity).adapter.isSwiped)
            return
        (context as MainActivity).adapter.isSwiped = true
        val holder = viewHolder as NoteDelegate.NoteHolder
        holder.onSwiped(direction)
    }
}