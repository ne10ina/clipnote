package com.ne10ina.clipnote

import android.app.Application
import com.hawkcatcherkotlin.akscorp.hawkcatcherkotlin.HawkExceptionCatcher

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        var exceptionCatcher = HawkExceptionCatcher(this, "dc9e6394-b4fb-4d3a-9aed-bfca524d82d4")
        exceptionCatcher.start()
    }
}