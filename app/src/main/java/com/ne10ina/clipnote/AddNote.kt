package com.ne10ina.clipnote

import android.animation.ValueAnimator
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.ConfigurationCompat
import com.ne10ina.clipnote.delegateadapters.RecyclerItems
import com.ne10ina.clipnote.keyboardchecker.KeyboardHeightObserver
import com.ne10ina.clipnote.keyboardchecker.KeyboardHeightProvider
import com.jaredrummler.android.colorpicker.ColorPickerDialog
import com.jaredrummler.android.colorpicker.ColorPickerDialogListener
import kotlinx.android.synthetic.main.activity_add_note.*
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*


class AddNote : AppCompatActivity(), KeyboardHeightObserver, ColorPickerDialogListener {
    private lateinit var keyboardHeightProvider: KeyboardHeightProvider
    private var keyboardHeight: Int = 0
    private lateinit var localTime: String
    lateinit var item: RecyclerItems.NoteItem
    private var year = 0
    private var month = 0
    private var day = 0
    private var hour = 0
    private var minute = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_note)
        colorLayout.setOnClickListener { showColorPicker() }

        keyboardHeightProvider = KeyboardHeightProvider(this)

        val dateFormat = SimpleDateFormat("HH:mm", ConfigurationCompat.getLocales(resources.configuration)[0])
        item = intent.getSerializableExtra("item") as RecyclerItems.NoteItem


        if (item.time != 0.toLong()) {
            inputNote.setText(item.text, TextView.BufferType.EDITABLE)

            if (item.type == Type.PINED) {

                radio_pin.isChecked = true
                typeText.text = getString(R.string.type_pin)
            } else if (item.type == Type.ONE) {

                radio_once.isChecked = true
                val dateFormat =
                    SimpleDateFormat(
                        "dd.MM.yy в HH:mm",
                        ConfigurationCompat.getLocales(this.resources.configuration)[0]
                    )
                typeText.text = getString(R.string.type_one) + " " + dateFormat.format(item.timeToDo)
            }
        } else {
            item.time = Calendar.getInstance().timeInMillis
        }

        colorLayout.setBackgroundColor(item.color)
        localTime = dateFormat.format(Calendar.getInstance().time)
        colorButton.setOnClickListener { showColorPicker() }
        textViewTime.text = localTime
        textViewSave.setOnClickListener { saveData() }
        buttonBack.setOnClickListener { onBackPressed() }
        backTextView.setOnClickListener { onBackPressed() }
    }

    private fun setUp() {
        Handler().postDelayed({
            keyboardHeightProvider.start()
            inputNote.isFocusableInTouchMode = true
            inputNote.requestFocus()

            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(inputNote, InputMethodManager.SHOW_IMPLICIT)

        }, 300)
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun saveData() {
        textViewSave.isEnabled = false
        textViewSave.setOnClickListener { }
        if (inputNote.getText().toString().trim().length == 0) {
            textViewSave.isEnabled = true
            textViewSave.setOnClickListener { saveData() }
            Toast.makeText(this, getString(R.string.noText), Toast.LENGTH_SHORT).show()
            return
        }
        val resultIntent = Intent()
        item.text = inputNote.text.toString()

        if (intent.getIntExtra("pos", -1) != -1) {
            with(resultIntent) {
                putExtra("oldT", intent.getStringExtra("oldT"))
                putExtra("pos", intent.getIntExtra("pos", 0))
                putExtra("oldS", intent.getSerializableExtra("oldS"))
                putExtra("oldC", intent.getIntExtra("oldC", Color.parseColor("#ffffff")))
            }
        }

        resultIntent.putExtra("item", item as Serializable)
        setResult(Activity.RESULT_OK, resultIntent)
        finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    private fun validateTime(): Boolean {
        if (getCalendar(day, month, year, hour, minute).timeInMillis - item.time < 10000) {
            Toast.makeText(this, getString(R.string.correctTime), Toast.LENGTH_SHORT).show()

            radio_default.isChecked = true
            typeText.text = getString(R.string.type_default)

            item.type = Type.DEFAULT
            item.timeToDo = -1

            return false
        } else {
            item.timeToDo = getCalendar(day, month, year, hour, minute).timeInMillis
            item.type = Type.ONE

            val dateFormat =
                SimpleDateFormat(
                    "dd.MM.yy в HH:mm",
                    ConfigurationCompat.getLocales(this.resources.configuration)[0]
                )

            radio_once.isChecked = true
            typeText.text = getString(R.string.type_one) + " " + dateFormat.format(Date(item.timeToDo))
            return true
        }
    }

    private fun datePicker() {
        hideKeyboard(this@AddNote)
        val c = Calendar.getInstance()
        year = c.get(Calendar.YEAR)
        month = c.get(Calendar.MONTH)
        day = c.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                timePicker()
                day = dayOfMonth
                this.year = year
                month = monthOfYear
            }, year, month, day
        )
        datePickerDialog.show()
    }

    private fun timePicker() {
        val calendar = Calendar.getInstance()
        hour = calendar.get(Calendar.HOUR_OF_DAY)
        minute = calendar.get(Calendar.MINUTE)

        val timePickerDialog =
            TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { _: TimePicker, i: Int, i1: Int ->
                hour = i
                minute = i1

                validateTime()

            }, hour, minute, true)
        timePickerDialog.show()
    }

    private fun getCalendar(day: Int, month: Int, year: Int, hour: Int, minute: Int): Calendar {
        val date = Calendar.getInstance()
        with(date) {
            set(Calendar.YEAR, year)
            set(Calendar.MONTH, month)
            set(Calendar.DAY_OF_MONTH, day)
            set(Calendar.HOUR_OF_DAY, hour)
            set(Calendar.MINUTE, minute)
        }
        return date
    }

    private fun showColorPicker() {
        hideKeyboard(this@AddNote)
        ColorPickerDialog.newBuilder().setColor(item.color).show(this)
    }

    override fun onColorSelected(dialogId: Int, color: Int) {
        item.color = color
        colorLayout.setBackgroundColor(color)
    }

    override fun onDialogDismissed(dialogId: Int) {

    }

    private fun showSecuritySettings() {
        val intent = Intent("com.meizu.safe.security.SHOW_APPSEC")
        intent.addCategory(Intent.CATEGORY_DEFAULT)
        intent.putExtra("packageName", BuildConfig.APPLICATION_ID)
        startActivity(intent)
    }

    private fun showDialog() {
        val alertDialog = AlertDialog.Builder(this).create()
        alertDialog.setTitle("Разрешения")
        alertDialog.setMessage("На телефонах Meizu нужно разрешить работу в фоне, иначе уведомления могу не приходить")
        alertDialog.setButton(
            AlertDialog.BUTTON_POSITIVE, "Разрешить"
        ) { _, _ ->
            showSecuritySettings()
            datePicker()
        }
        alertDialog.setButton(
            AlertDialog.BUTTON_NEGATIVE, "Закрыть"
        ) { dialog, _ ->
            dialog.dismiss()
            datePicker()
        }
        alertDialog.show()
    }

    fun onRadioButtonClicked(view: View) {
        if (view is RadioButton) {

            if (item.state == State.ARCHIVED) {
                Toast.makeText(this, getString(R.string.noteArchived), Toast.LENGTH_SHORT).show()
                radio_default.isChecked = true
                return
            }

            val checked = view.isChecked

            when (view.getId()) {
                R.id.radio_default ->
                    if (checked) {
                        typeText.text = getString(R.string.type_default)
                        item.type = Type.DEFAULT
                        item.state = State.ACTIVE
                        item.timeToDo = -1
                    }
                R.id.radio_pin ->
                    if (checked) {
                        typeText.text = getString(R.string.type_pin)
                        item.type = Type.PINED
                        item.state = State.PINED
                        item.timeToDo = -1
                    }
                R.id.radio_once ->
                    if (checked) {
                        radio_default.isChecked = true
                        typeText.text = getString(R.string.type_default)
                        item.type = Type.DEFAULT
                        val sharedPref = getSharedPreferences("myPrefs", Context.MODE_PRIVATE)
                        val isDialogShown = sharedPref.getBoolean("show", false)
                        if (android.os.Build.MANUFACTURER == "Meizu" && !isDialogShown) {
                            with(sharedPref.edit())
                            {
                                putBoolean("show", true)
                                apply()
                            }
                            showDialog()
                            return
                        }
                        datePicker()
                    }
            }
        }
    }

    override fun onKeyboardHeightChanged(height: Int, orientation: Int) {
        if (height == 0) {
            animate(height, 0)
        } else {
            animate(0, height)
        }
        keyboardHeight = height
    }

    private fun animate(positionFrom: Int, positionTo: Int) {
        val animator: ValueAnimator = ValueAnimator.ofInt(positionFrom, positionTo)
        val params = vv.layoutParams as ConstraintLayout.LayoutParams

        animator.addUpdateListener { valueAnimator ->
            params.bottomMargin = valueAnimator.animatedValue as Int
            vv.requestLayout()
        }

        animator.interpolator = DecelerateInterpolator()
        animator.duration = 300
        animator.start()
    }

    override fun onResume() {
        super.onResume()
        setUp()
        keyboardHeightProvider.setKeyboardHeightObserver(this)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    override fun onPause() {
        super.onPause()
        animate(keyboardHeight, 0)
    }

    override fun onStop() {
        super.onStop()
        keyboardHeightProvider.close()
    }
}
