package com.ne10ina.clipnote

import java.io.Serializable

enum class State : Serializable {
    PINED,
    ACTIVE,
    ARCHIVED
}

enum class Type {
    DEFAULT,
    PINED,
    ONE,
    PERIODIC
}