package com.ne10ina.clipnote.utils

import android.app.*
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Handler
import android.util.DisplayMetrics
import android.widget.RemoteViews
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import com.ne10ina.clipnote.*
import com.ne10ina.clipnote.db.AppDatabase
import com.ne10ina.clipnote.delegateadapters.RecyclerItems
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class Utils {
    companion object {

        fun deleteItem(item: RecyclerItems.NoteItem, db: AppDatabase) {
            GlobalScope.launch(Dispatchers.IO) {
                runBlocking(Dispatchers.IO) {
                    db.noteDao().deleteNote(item)
                }
            }
        }

        fun updateItem(newItem: RecyclerItems.NoteItem, db: AppDatabase) {
            GlobalScope.launch(Dispatchers.IO) {
                runBlocking(Dispatchers.IO) {
                    db.noteDao().updateNote(newItem)
                }
            }
        }

        fun addItem(item: RecyclerItems.NoteItem, db: AppDatabase, context: Context) {
            GlobalScope.launch(Dispatchers.IO) {
                runBlocking(Dispatchers.IO) {
                    val id = db.noteDao().addNote(item).toInt()
                    val activity = context as MainActivity
                    item.id = id
                    activity.list[item.state]!!.remove(item)
                    activity.list[item.state]!!.add(1, item)
                }
            }
        }

        fun createNotificationChannel(context: Context) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val name = "123"
                val descriptionText = "123"
                val importance = NotificationManager.IMPORTANCE_DEFAULT
                val channel = NotificationChannel("123", name, importance).apply {
                    description = descriptionText
                }

                val notificationManager: NotificationManager =
                    context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.createNotificationChannel(channel)
            }
        }

        fun deleteNotification(id: Int, context: Context) {
            val mNotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            mNotificationManager.cancel(id)
        }

        fun addStableNotification(text: String, id: Int, color: Int, context: Context) {
            val notificationLayout = RemoteViews(context.packageName, R.layout.notification)
            notificationLayout.setTextViewText(R.id.notificationText, text)
            notificationLayout.setInt(R.id.colorLayout, "setBackgroundColor", color)

            val builder = NotificationCompat.Builder(context, "123")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setSmallIcon(R.drawable.pin)
                .setCustomContentView(notificationLayout)
                .setContent(notificationLayout)
                .setOngoing(true)
                .setSortKey("com.ne10ina.clipnote")
                .setGroup("com.ne10ina.clipnote")

            val notificationIntent = Intent(context, MainActivity::class.java)

            val contentIntent = PendingIntent.getActivity(
                context, 123, notificationIntent,
                PendingIntent.FLAG_ONE_SHOT
            )
            builder.setContentIntent(contentIntent)

            val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.notify(id, builder.build())
            //manager.notify(228, summaryNotification(context))
        }

        fun summaryNotification(context: Context): Notification {
            val summaryNotification = NotificationCompat.Builder(context, "123")
                //set content text to support devices running API level < 24
                .setContentText("Two new messages")
                .setSmallIcon(R.drawable.pin)
                //build summary info into InboxStyle template
                .setGroup("com.ne10ina.clipnote")
                .build()

            return summaryNotification
        }

        fun addNotification(text: String, id: Int, color: Int, context: Context) {
            val notificationLayout = RemoteViews(context.packageName, R.layout.notification)
            notificationLayout.setTextViewText(R.id.notificationText, text)
            notificationLayout.setInt(R.id.colorLayout, "setBackgroundColor", color)

            val builder = NotificationCompat.Builder(context, "123")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setSmallIcon(R.drawable.pin)
                .setCustomContentView(notificationLayout)
                .setContent(notificationLayout)
                .setGroup("com.ne10ina.clipnote")
                .setSortKey("com.ne10ina.clipnote")

            val notificationIntent = Intent(context, MainActivity::class.java)

            val contentIntent = PendingIntent.getActivity(
                context, 123, notificationIntent,
                PendingIntent.FLAG_ONE_SHOT
            )
            builder.setContentIntent(contentIntent)

            val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.notify(id, builder.build())
        }

        fun addTask(context: Context, id: Int, text: String, color: Int, time: Long) {
            val intent = Intent(context, BroadcastReceiver::class.java)

            intent.putExtra("code", 1)
            intent.putExtra("text", text)
            intent.putExtra("id", id)
            intent.putExtra("color", color)

            val pendingIntent = PendingIntent.getBroadcast(
                context.applicationContext, id, intent, PendingIntent.FLAG_UPDATE_CURRENT
            )

            val alarmManager = context.getSystemService(AppCompatActivity.ALARM_SERVICE) as AlarmManager
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, time, pendingIntent)
            else
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, time, pendingIntent)
        }

        fun deleteTask(context: Context, text: String, id: Int, color: Int) {
            val intent = Intent(context, BroadcastReceiver::class.java)

            intent.putExtra("code", 1)
            intent.putExtra("text", text)
            intent.putExtra("id", id)
            intent.putExtra("color", color)

            val pendingIntent = PendingIntent.getBroadcast(
                context, id, intent, PendingIntent.FLAG_UPDATE_CURRENT
            )

            val alarmManager = context.getSystemService(AppCompatActivity.ALARM_SERVICE) as AlarmManager
            alarmManager.cancel(pendingIntent)
        }

        fun smoothScroll(context: Context, toPosition: Int, recyclerView: RecyclerView) {
            Handler().postDelayed({
                val smoothScroller = object : LinearSmoothScroller(context) {
                    override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics): Float {
                        return 100f / displayMetrics.densityDpi
                    }
                }
                smoothScroller.targetPosition = toPosition
                (recyclerView.layoutManager as LinearLayoutManager).startSmoothScroll(smoothScroller)
            }, 500)
        }

        fun dpToPx(dp: Int): Int = (dp * Resources.getSystem().displayMetrics.density).toInt()

        fun getPositionToScroll(state: State, activity: MainActivity): Int =
            when (state) {
                State.PINED -> 0
                State.ACTIVE -> activity.list[State.PINED]!!.size
                State.ARCHIVED -> activity.list[State.PINED]!!.size + activity.list[State.ACTIVE]!!.size + 1
            }

        fun getIcon(type: Type, context: Context): Drawable =
            when (type) {
                Type.PINED -> ContextCompat.getDrawable(context, R.drawable.ic_pin_black_32dp)
                Type.ONE -> ContextCompat.getDrawable(context, R.drawable.ic_timer_gray_32dp)
                else -> ContextCompat.getDrawable(context, R.drawable.ic_speaker_notes_black_32dp)
            }!!

    }
}